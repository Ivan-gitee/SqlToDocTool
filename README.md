# SqlToDocTool
这是一款简单的数据库文档生成工具，主要实现了 SQlServer+MYsql 说明文档的生成，并且支持 SQLServer 数据库的备份功能，主要可以把数据库的表以及表的详细字段信息导出到 Word 中，可以方便开发人员了解数据库的信息或写技术说明文档。

界面效果
1. 主界面

![输入图片说明](https://images.gitee.com/uploads/images/2020/0407/202104_0c2c61cd_335359.png "主界面")


2. 保存生成的文档

![输入图片说明](https://images.gitee.com/uploads/images/2020/0407/202126_bdc8ac71_335359.png "生成word文件")


3. SQLsever 生成的文档效果

![输入图片说明](https://images.gitee.com/uploads/images/2020/0407/202151_8a3ecc32_335359.png "生成SQlServer文档")


4. MYSQL生成的文档效果

![输入图片说明](https://images.gitee.com/uploads/images/2020/0407/202215_6b093191_335359.png "mysql版本")


5. 备份数据界面

![输入图片说明](https://images.gitee.com/uploads/images/2020/0407/202243_e2d3efcf_335359.png "在这里输入图片标题")


6. 导出的 bak 文件

![输入图片说明](https://images.gitee.com/uploads/images/2020/0407/202254_d71ccb68_335359.png "屏幕截图.png")

目前功能还不够完善，该软件后续会持续改进，欢迎各位大神提建议！

微信:hgmyzhl

邮箱：hgmyz@outlook.com
微信公众号：
技术号：

![输入图片说明](https://images.gitee.com/uploads/images/2020/0407/202522_9f003c7c_335359.png "技术号.png")

生活号：

![输入图片说明](https://images.gitee.com/uploads/images/2020/0407/202540_81d629a5_335359.png "生活号.png")